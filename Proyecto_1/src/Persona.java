
public class Persona {
	private Comunidad comunidad;
    private int _id;
	private Enfermedad enfermedad;
    private boolean estado; 
    
    public Persona (int id){
    	/**
    	 * Constructor de la clase Persona
    	 * solo recibe el id de la persona
    	 * estado se refiere a si la persona es inmune o vulnerable
    	 * inmune (true): ya superó la enfermedad
    	 * vulnerable (false): no ha superado la enfermdedad, esto se da
    	 * por dos casos, no ha estado enfermo o sigue enfermo*/
    	this._id = id;
    	this.estado = false;
 
    	
    }
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public Comunidad getComunidad() {
		return comunidad;
	}

	public void setComunidad(Comunidad comunidad) {
		this.comunidad = comunidad;
	}
	public Enfermedad getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(Enfermedad enfermedad) {
		this.enfermedad = enfermedad;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}	

}

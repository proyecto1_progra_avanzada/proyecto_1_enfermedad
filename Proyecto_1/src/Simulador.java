
import java.util.Collections;

public class Simulador {
	private Comunidad com;
	
	Simulador(Comunidad c){
		
		/**
		 * Constructor de la clase simulador
		 * Recibe a la clase Comunidad como su atributo, en la cual se desarrollará la enfermedad
		 * 
		 * contagiando() se encarga de contagiar a los primeros infectados definidos en el main
		 * yaAvanzaElContador() se encargará de inicializar su contador de días enfermos de cada persona
		 * */
		this.com = c;
		System.out.println("Total de habitantes de la comunidad: "+ com.getNum_cuidadanos() );
		
		
		System.out.println("Contagiados iniciales: " + com.getNum_infectados());
		

		contagiando(com.getNum_infectados());
		yaAvanzaElContador();
		
		
	
	}
	

	public Comunidad getCom() {
		return com;
	}

	public void setCom(Comunidad com) {
		this.com = com;
	}
	
	
	public double nContagiadosPorDia(double infectados_activos){//double xcf, double pcf, double ip,  {
		/**función: devolver el número de conagios nuevos de cada día (double)
		 * 
		 * ¿Cómo?
		 * 
		 * xcf = promedio conexión fisica
		 * pcf = probabilidad conexión fisica
		 * ip = infección probable
		 * xcf * pcf = las personas que un contagiado ve en promedio en el día que son contacto estrecho
		 * xcf * pcf * ip = de esas personas que son contacto estrecho, cuantas se contagian
		 * xcf * pcf * ip * infectados_activos = el total de personas que se contagian por los activos
		 * 
		 * Condición:
		 * se redondea el resultado final, ya que una persona no puede contagiarse por partes 
		 */
	
		double xcf = com.getPromedio_conexion_fisica();
		double pcf = com.getProbabilidad_conexion_fisica();
		double ip = com.getEnfermedad().getInfeccion_probable();
		
		double infectados_por_dia = xcf * pcf * ip * infectados_activos;

		return Math.round(infectados_por_dia);  
	}
	
	
	
	public double contagiando( double contagios_diarios) {
		Collections.shuffle(com.getComunidad());
		/**Función: enfermar a las personas y devolvercuantas se enfermaron ese día
		 * 
		 * ¿Cómo? vuelve verdaero el atributo enfermo.
		 * Esto pertenece a la enfermedad que fue asignada a cada persona
		 * 
		 * Condición: que la persona no sea inmune ni esté enferma
		 * */
		int cont = 0;
		for(Persona p: com.getComunidad()) {
			if(!p.isEstado() && !p.getEnfermedad().isEnfermo()) {
				p.getEnfermedad().setEnfermo(true);
				cont++;

			}
			if (cont == contagios_diarios) {
				break;
				
			}
		}
		
		return cont;
	}
	
	public void yaAvanzaElContador() {
		/**
		 * Función: que el contador de cada persona avance 
		 * 
		 * ¿Cómo? 
		 * El contador de cada persona indica los días que lleva enfermo
		 * Este tiene que avanzar cada vez que pase un día
		 * Enonces se le debe sumar 1 al valor que tiene el contador cada día
		 * 
		 * Condiciones:
		 * el contador solo va a avanzar si la persona no es inmune (Estado is false)
		 * y está enfermo (Enfermo is true)
		 * 
		 * 
		 * Función 2: convertir a las personas a inmunes
		 * 
		 * ¿Cómo?
		 * Compara el nuevo valor del contador de cada persona con el promdeio de pasos para recuperarse
		 * si el contador es mayor, la persona se vuelve inmune (Estado is true) y deja de estar enferma
		 * (Enfermo is false)
		 * 
		 * Condición:
		 * solo aplica a personas enfermas, ya que el contador no avanza si no se han contagiado 
		 * 
		 * 
		 * */
		for (Persona x: com.getComunidad()) {
			if(!x.isEstado() && x.getEnfermedad().isEnfermo()) {
				x.getEnfermedad().setContador(x.getEnfermedad().getContador() + 1);
			}
			
			if(x.getEnfermedad().getContador()> com.getEnfermedad().getPromedio_pasos()) {
				x.setEstado(true);
				x.getEnfermedad().setEnfermo(false);
			}

		}
	}
	
	public int cuenta_activos() {
		/**
		 * Función: contar y devolver los casos activos
		 * 
		 * ¿Cómo?
		 * Tiene un contador que se inicializa en 0
		 * Recorre el ArrayList que contiene a los objetos de clase persona
		 * Cuando encuentra a uno enfermo (enfermo is true), el contador avanza
		 * 
		 * Condición:
		 * Solo hay un caso en el que atributo enfermo es verdadero
		 * Que es cuando el contador no sobrepasa al promedio de pasos para recuperarse
		 * Esto significa que unicamente los activos están enfermos 
		 * */
		int cont1 = 0; 
		for (Persona p: com.getComunidad()) {
			if (p.getEnfermedad().isEnfermo()) {
				cont1++;
			}
			
		}
		
		return cont1;
	}
	

	
	
	public void run(int pasos) {
		System.out.println("");
		/**
		 * Función: Representar los días que pasan después de que 
		 * 			llegan personas contagiadas a la comunidad
		 * 
		 * ¿Cómo?
		 * Primero se definen las variables que se irán modificando conforme avanza el ciclo,
		 * 2 e ellas inician con el mismo valor.
		 * Segundo, el método solo recibe la variable pasos, que representa los días, 
		 * entonces se inicia un ciclo for que recorra solamente esos días.
		 * Tercero, ser ordenan los metodos definidos anteriormente 
		 * de modo que cada uno devuleva el valor a la variable que correspode 
		 * el orden es:
		 * 				1° Calcular los contagios según los activos  --> c_actuales = nContagiadosPorDia(activos);
		 * 				2° Enfermar a ese n° de personas  --> c_actuales_reales = contagiando(c_actuales);
		 * 				3° Inicializar el contador de cada una de esas personas 
		 * 					o detenerlo después de los días correspondientes  -->  yaAvanzaElContador();
		 * 				4° Contar lod activos del día, 
		 * 					depende de los cambiós que haga yaAvanzaElContador()  --> activos = cuenta_activos();
		 * 				5° Calcular los contagios totales, es la suma de todos los contagiados que han habido
		 * 					--> contagios_totales= contagios_totales + c_actuales_reales;
		 * 				6° Imprimir los nuevos contagios, los contagios totales y los casos activos
		 * 					--> System.out.println("Contagios del dia "+ dias+ ": "+ c_actuales_reales+
								"	Contagios totales: "+ contagios_totales + "		Acticvos: " + activos);
		 * */
		double contagios_totales = com.getNum_infectados(); 
		double c_actuales, c_actuales_reales;
		double activos = contagios_totales;
		for(int i = 0; i<pasos; i++) {
			
			c_actuales = nContagiadosPorDia(activos);
			c_actuales_reales = contagiando(c_actuales);
			yaAvanzaElContador();
			activos = cuenta_activos();
			contagios_totales= contagios_totales + c_actuales_reales;
			
			int dias = i + 1;
			System.out.println("Contagios del dia "+ dias+ ": "+ c_actuales_reales+
					"	Contagios totales: "+ contagios_totales + "		Acticvos: " + activos);
		}
		
	}
	
	
	

}


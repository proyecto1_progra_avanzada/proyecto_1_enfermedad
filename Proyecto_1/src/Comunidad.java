import java.util.ArrayList;

public class Comunidad {
	private double num_cuidadanos;
	private double  promedio_conexion_fisica;
	private Enfermedad enfermedad;
	private double  num_infectados;
	private double probabilidad_conexion_fisica;
	
	private ArrayList<Persona> comunidad = new ArrayList<Persona>();
	
	
	public Comunidad(double habitantes, double  xcf,  double  infectados, double pcf, Enfermedad e){ 
		/**
		 * Constructor de la clase Comunidad
		 * Recibe la cantidad de habitantes, el promedio de conexiones físicas, los infectados,
		 * la probabilidad de conecxiones físicas y la enfermedad (primer constructor)
		 * 
		 * Además llama al método lasPersonas() con el número de habitantes para crear 
		 * y agregar personas a la comunidad
		 * */
		this.num_cuidadanos = habitantes;
		this.promedio_conexion_fisica = xcf;
		this.enfermedad = e;
		this.num_infectados = infectados;
		this.probabilidad_conexion_fisica = pcf;
		
		
		
		lasPersonas(this.num_cuidadanos);
		
	}
	
	public double getNum_cuidadanos() {
		return num_cuidadanos;
	}
	public void setNum_cuidadanos(double num_cuidadanos) {
		this.num_cuidadanos = num_cuidadanos;
	}
	public double getPromedio_conexion_fisica() {
		return promedio_conexion_fisica;
	}
	public void setPromedio_conexion_fisica(double promedio_conexion_fisica) {
		this.promedio_conexion_fisica = promedio_conexion_fisica;
	}
	public Enfermedad getEnfermedad() {
		return enfermedad;
	}
	public void setEnfermedad(Enfermedad enfermedad) {
		this.enfermedad = enfermedad;
	}
	public double getNum_infectados() {
		return num_infectados;
	}
	public void setNum_infectados(double  num_infectados) {
		this.num_infectados = num_infectados;
	}
	public double getProbabilidad_conexion_fisica() {
		return probabilidad_conexion_fisica;
	}
	public void setProbabilidad_conexion_fisica(double probabilidad_conexion_fisica) {
		this.probabilidad_conexion_fisica = probabilidad_conexion_fisica;
	}

	public ArrayList<Persona> getComunidad() {
		return comunidad;
	}

	public void addPersona(Persona p) {
		comunidad.add(p);
	}
	
	public void lasPersonas(double habitantes) {
		/**
		 * 
		 * 1° Se crea un n° aleatorio  entre 1 y 1000 y se convierte a int
		 * 	  Este va a ser el n° inicial de los id, llamado inicio
		 * 2° se crea una variable int llamada final_ 
		 * 	  esta es igual a la variable inicio más el total de habitantes
		 * 	  esta va a ser el tope del ciclo for
		 * 3° Se crea  el ciclo for
		 *    con i = inicio y i < final_
		 *    i va a significar el id de las personas
		 * 4° Después de creada la persona se crea una enfermedad (segundo constructor)
		 *    y se agrega como atributo a la persona
		 * 5° La persona completa se agrega al ArrayList creado al inicio de la clase 
		 * 
		 *    */
		
		System.out.println("");
		
		int inicio = (int)(Math.random()*1000+1); 
		
		
		double final_ = inicio + habitantes;
		
		for(int i = inicio; i<final_; i++) {
			Persona persona = new Persona(i);
			Enfermedad ee = new Enfermedad();
			persona.setEnfermedad(ee);
			addPersona(persona);
		}
				
	}
	
}

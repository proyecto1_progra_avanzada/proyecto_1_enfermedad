
public class Enfermedad {
	private double infeccion_probable;
	private double promedio_pasos;
	private boolean enfermo; 
	private int contador;
	
	public Enfermedad(double ip, double pp) {
		/**
		 * Constructor  de Enfermedad utilizado para la clase Comunidad
		 * Recibe los valores de infección probable y pasos para recuperarse */
		this.infeccion_probable = ip;
		this.promedio_pasos = pp;
		
	}
	
	public Enfermedad() {
		/**
		 * Constructor de Enfermedad utilizado para la clase Persona
		 * no recibe nada, ya que en este punto la persona no está enferma (enfermo is false)
		 * y el contador solo avanza si el atributo enfermo es verdadero
		 *  */
		
		this.enfermo = false;
		this.contador = 0;
		
	}
	public double getPromedio_pasos() {
		return promedio_pasos;
	}
	public void setPromedio_pasos(double promedio_pasos) {
		this.promedio_pasos = promedio_pasos;
	}
	public int getContador() {
		return contador;
	}
	public void setContador(int contador) {
		this.contador = contador;
	}
	public double getInfeccion_probable() {
		return infeccion_probable;
	}
	public void setInfeccion_probable(double infeccion_probable) {
		this.infeccion_probable = infeccion_probable;
	}
	public boolean isEnfermo() {
		return enfermo;
	}
	public void setEnfermo(boolean enfermo) {
		this.enfermo = enfermo;
	}
}

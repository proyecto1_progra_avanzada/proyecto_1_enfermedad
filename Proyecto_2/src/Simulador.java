
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import Enfermedades.Afeccion;
import Enfermedades.Base;
import Principal.Comunidad;
import Principal.Persona;
import Vacunas.Vacuna;
import Vacunas.control_vacunas;


public class Simulador implements control_vacunas{
	private Comunidad com;
	private ArrayList<Base> e_b_existentes;
    private ArrayList<Afeccion> a_existentes;
    private ArrayList<Persona> _muertos;
    private HashMap<Integer, Vacuna> vacunas;
	
	Simulador(Comunidad c){
		
		this.e_b_existentes= new ArrayList<Base>();
		this.a_existentes = new ArrayList<Afeccion>();
		this._muertos = new ArrayList<Persona>();
		
		this.vacunas = new HashMap<Integer, Vacuna>();
		/**
		 * Constructor de la clase simulador
		 * Recibe a la clase Comunidad como su atributo, en la cual se desarrollará la enfermedad
		 * 
		 * Al mismo tiempo se crean los ArrayList:
		 * 	-	e_b_existentes --> para todas las enfermedades de base.
		 *  -	a_existentes   --> para todas las afecciones.
		 *  - 	_muertos	   --> para guaradar los objetos tipo Persona muertos. 
		 *  
		 * Y el HashMap vacunas que guarda lor 3 tipos de vacuna
		 *  
		 * No está presente ninguna función ya que dependen de los ArrayList y estos se rellenan en el main
		 * 
		 * */
		this.com = c;
		
		
	
	}
	
	public void pre_run() {
		/**
		 * 
		 * llama a los métodos que originalmente estaban en el constructor 
		 * y algunos extra que son importantes como :
		 *  	enfermedades_existentes(); 
		 *  	probabilidadDeEnfenfar();
		 *  	gravedad(); 
		 *  
		 *  y un for para establecer la cantidad de vacunas que va a necesitar la comunidad
		 *  	
		 * contagiando() se encarga de contagiar a los primeros infectados definidos en el main
		 * yaAvanzaElContador() se encargará de inicializar su contador de días enfermos de cada persona
		 * */
		enfermedades_existentes();
		probabilidadDeEnfenfar();
		
		for(Vacuna v: vacunas.values()) {
			cantidad_de_vacunas(v);
			
		}
		System.out.println("Total de habitantes de la comunidad: "+ com.getNum_cuidadanos() );
		
		
		System.out.println("Contagiados iniciales: " + com.getNum_infectados());
		
		//Collections.shuffle(com.getComunidad());
		
		contagiando(com.getNum_infectados());
		yaAvanzaElContador();
		gravedad();
	}

	public Comunidad getCom() {
		return com;
	}

	public void setCom(Comunidad com) {
		this.com = com;
	}
	
	
	public double nContagiadosPorDia(double infectados_activos){//double xcf, double pcf, double ip,  {
		/**función: devolver el número de conagios nuevos de cada día (double)
		 * 
		 * ¿Cómo?
		 * 
		 * xcf = promedio conexión fisica
		 * pcf = probabilidad conexión fisica
		 * ip = infección probable
		 * dn = n° aleatorio para poder discriminar de forma real a la población
		 * xcf * pcf = las personas que un contagiado ve en promedio en el día que son contacto estrecho
		 * xcf * pcf * ip = de esas personas que son contacto estrecho, cuantas se contagian
		 * xcf * pcf * ip * infectados_activos = el total de personas que se contagian por los activos
		 * xcf * pcf * ip * infectados_activos * dn = el total real de personas que se contagian por los activos
		 * 
		 * real significa que algunos contagios pueden ser comunes entre activos
		 * ej: 1 activo contagia a 2 y otro a 3 más pero 2 de esos 3 son los mismos 2 del primero
		 * 
		 * Condición:
		 * se redondea el resultado final, ya que una persona no puede contagiarse por partes 
		 */
	
		double xcf = com.getPromedio_conexion_fisica();
		double pcf = com.getProbabilidad_conexion_fisica();
		double ip = com.getEnfermedad().getInfeccion_probable();
		
		double dn = Math.random()*1;
		
		double infectados_por_dia = xcf * pcf * ip * infectados_activos * dn;

		return Math.round(infectados_por_dia);  
	}
	
	
	
	public double contagiando( double contagios_diarios) {
		/**Función: enfermar a las personas y devolvercuantas se enfermaron ese día
		 * 
		 * ¿Cómo? vuelve verdaero el atributo enfermo.
		 * Esto pertenece a la enfermedad que fue asignada a cada persona
		 * 
		 * Condición: que la persona no sea inmune ni esté enferma
		 * 
		 * 
		 * Función 2: sumar 1 al indice de gravedad para que los que ya tengan 
		 * 				una base puedan estar graves o morir
		 * 				Pero con la condicion que si están vacunados con V1 
		 * 				solo se le suma 0.75 que corresponde a bajar en 25 % la intencidad
		 * 				de la enfermedad infecciosa, consiferando que al momento de vacunarse
		 * 				también se bajó un 25 % el idice de gravedad original 
		 * 
		 * ¿Cómo? las personas solo se contagian una vez
		 * entonces se aprovecha ese momento para sumar 1 al i_g
		 * */
		int cont = 0;
		for(Persona p: com.getComunidad()) {
			if(!p.isEstado() && !p.isEnfermo()) {
				p.setEnfermo(p.getEnfermedad().isEnfermo());
				p.setCont(p.getEnfermedad().getContador());
				
				//Como la persona solo se contagia una vez se le suma 1 para cumplir con la condicion de gravedad
				if(p.getV() != null && p.getV().getTipo() == 1) {
					//System.out.println("**************************Persona con v1, i_g original = " + p.getIndice_gravedad());
					p.setIndice_gravedad(p.getIndice_gravedad()+0.75);
					//System.out.println("i_g nuevo = " + p.getIndice_gravedad());
				}else{
					
					p.setIndice_gravedad(p.getIndice_gravedad()+1);
				}
				cont++;

			}
			if (cont == contagios_diarios) {
				break;
				
			}
		}
		
		return cont;
	}
	
	public void yaAvanzaElContador() {
		/**
		 * Función: que el contador de cada persona avance 
		 * 
		 * ¿Cómo? 
		 * El contador de cada persona indica los días que lleva enfermo
		 * Este tiene que avanzar cada vez que pase un día
		 * Enonces se le debe sumar 1 al valor que tiene el contador cada día
		 * 
		 * Condiciones:
		 * el contador solo va a avanzar si la persona no es inmune (Estado is false)
		 * y está enfermo (Enfermo is true)
		 * 
		 * */
		
		for (Persona x: com.getComunidad()) { 
			if(!x.isEstado() && x.isEnfermo()) { //Persona infectada
				x.setCont(x.getCont() + 1);
			}
		}
	}
	
	public int cuenta_activos() {
		/**
		 * Función: contar y devolver los casos activos
		 * 
		 * ¿Cómo?
		 * Tiene un contador que se inicializa en 0
		 * Recorre el ArrayList que contiene a los objetos de clase persona
		 * Cuando encuentra a uno enfermo (enfermo is true) y no inmune (estado is false), el contador avanza
		 * 
		 * Condición:
		 * ahora hay 2 casos en el que atributo enfermo es verdadero
		 * cuando la persona está contagiada de forma grave o no
		 * entonces, va a devolver el valor total de activos 
		 * */
		int cont1 = 0; 
		for (Persona p: com.getComunidad()) {
			if (p.isEnfermo()&& !p.isEstado()) {
				// los casos activos tambien pueden ser graves
				cont1++;
			}
			
		}
		return cont1;
	}
	

	
	public void run(int pasos) {
		System.out.println("");
		/**
		 * Función: Representar los días que pasan después de que 
		 * 			llegan personas contagiadas a la comunidad
		 * 
		 * ¿Cómo?
		 * Primero se definen las variables que se irán modificando conforme avanza el ciclo,
		 * 3 de ellas inician con el mismo valor.
		 * Segundo, el método solo recibe la variable pasos, que representa los días, 
		 * entonces se inicia un ciclo for que recorra solamente esos días.
		 * Tercero, se ordenan los metodos definidos de modo que cada uno devuleva el valor a la variable que correspode 
		 * el orden es:
		 * 				1° Calcular los contagios según los activos no graves(contagiantes) --> c_actuales = nContagiadosPorDia(activos_no_graves);
		 * 				2° Enfermar a ese n° de personas  --> c_actuales_reales = contagiando(c_actuales);
		 * 				3° Inicializar el contador de cada una de esas personas -->  yaAvanzaElContador();
		 * 				4° contar los casos graves y detener el contador de las personas que mueren o se recuperan
		 * 					 después de los días correspondientes  --> graves = gravedad();
		 * 				5° Contar lo muertos del día --> muertos = muertos();
		 * 				6° Contar los recuperados del diá --> recuperados = recuperados();
		 * 				Estos 2 ultimos pasos dependen de los cambios que haga gravedad
		 * 				7° Contar los activos del día, tanto los contagiantes como los graves  --> activos = cuenta_activos();
		 * 				8° Calcular los casos contagiantes --> activos_no_graves = activos - graves;
		 * 				9° Calcular los contagios totales, es la suma de todos los contagiados que han habido a lo largo del ciclo
		 * 					--> contagios_totales= contagios_totales + c_actuales_reales;
		 * 				10° imprimir todo lo importante -->
		 * 				System.out.println("Contagios del dia "+ dias+ ": "+ c_actuales_reales+
					"	Contagios totales: "+ contagios_totales + "	activos" + activos +
					"	\ncasos contagiantes: " + activos_no_graves+"		graves(sin muertos ni recuperados): " + graves   + 
					"	\nMuertos: " + muertos + "	recuperados: " + recuperados);
					
					
			hay que considerar que antes del for se imprime el total de la población y
			se especifica la cantidad de la población que está: sano, con afeccion o con E. base.
			
			
			Para la vacunación se definen 4 variables nuevas, mitad_run, vac_act, vac_original y vacunados
			-	mitad_run representa la mitad de los pasos del metodo run, como numero entero
				e indica el momento en que llegan las vacunas a la población
			-	vac_act se encarga de contar las vacunas disponibles en cada ciclo
			-	vac_original solo se activa una vez para contar las vacunas originals que llegaron 
				a la cominidad, esto sirve para saber cuantas personas se han vacunado
			-	vacunados cuenta a las personas que llevan al menos 1 vacuna, esto se activa después del proceso de vacunación 
		 * 	La vacunación inicia con un if que pregunta si el ciclo va en la mitad o más
		 * 	si es así se pregunta si va justo en la mitad, entonces se imprime que llegaron las
		 * 	vacunas y se activa vac_original, luego se pregunta si quedan vacunas, si es que sí 
		 * 	continuan los metodos vacunar() y segundas_dosis() 
		 * 	aplican las vacunas y se vuelven a contar al final con vac_act = conteo_vac();
		 * 	Si es que no, se imprime que no hay vacunas
		 * 	
		 * 
		 * 
		 * Luego de terminar el ciclo, se llama a ciertas funciones:
		 * -	sacar_muertos() --> resta las personas muertas del total de la población
		 * -	impr_datos_poblacion(); --> imprime los datos finales de la población
		 * */
		double contagios_totales = com.getNum_infectados(); 
		double c_actuales, c_actuales_reales, graves, recuperados, muertos = 0;
		double activos = contagios_totales;
		double activos_no_graves = contagios_totales;
		
		int vac_act = 0, vac_original = 0;
		
		impr_datos_poblacion();
		for(int i = 0; i<pasos; i++) {
			System.out.println("\n\n");
			int mitad_run = (int)(pasos/2);
			int vacunados = 0;
			// vacunacion
			if(i >= mitad_run) {
				if(i == mitad_run) {
					System.out.println("\n\n******************************llegaron las vacunas**********************************\n\n");
					vac_original = conteo_vac();
					System.out.println("Total de vacunas disponibles para la poblaion: " + vac_original);
				}
				vac_act = conteo_vac();
				if(vac_act > 0) {
				System.out.println("Inicio proceso vacunacion dia: " + (i+1) + "*******************************");
				vacunar(i, mitad_run);
				segundas_dosis(i);
				vac_act = conteo_vac();
				System.out.println("cierre proceso vacunacion ****************************************");
				} else {
					System.out.println("No hay vacunas disponibles");
				}
			}
			
			System.out.println("");
			vacunados = cuenta_vacunados();
			System.out.println("Vacunas actuales: " + vac_act + " vacunados actualmente (con 1 o 2 dosis): "+ vacunados);
			
			c_actuales = nContagiadosPorDia(activos_no_graves);
			c_actuales_reales = contagiando(c_actuales);
			yaAvanzaElContador();
			graves = gravedad();
			muertos = muertos();
			recuperados = recuperados();
			activos = cuenta_activos();
			activos_no_graves = activos - graves;
	
			contagios_totales= contagios_totales + c_actuales_reales;
			int dias = i + 1;
			System.out.println("Contagios del dia "+ dias+ ": "+ c_actuales_reales+
					"	\nContagios totales: "+ contagios_totales + "		Activos: " + activos +
					"	\nCasos contagiantes: " + activos_no_graves+"		Graves: " + graves   + 
					"	\nMuertos: " + muertos + "			Inmunes: " + recuperados+
					"\nDato: inmunes incluye recuperados e inmunizados por V3");
					
		}
		
		sacar_muertos();
		impr_datos_poblacion();
		
	}
	
	public void promedioEdad() {
		/**calcula la edad promedio de las personas de la comunidad
		 * tambien imprime el total de la población 
		 * esta funcion se aplica antes y despues de run, 
		 * para evidenciar la diferencia.
		 * */
		 
		double prom = 0;
		for(Persona i: com.getComunidad()) {
			prom = prom + i.getEdad();
		}
		//System.out.println("total edades:" + prom);
		prom = prom/com.getComunidad().size();
		System.out.println("	Total personas:" + com.getComunidad().size() +
							"\n	El promedio de edad de la comunidad es: " + prom);
	}

	
	// Almacenamiento de todas las afecciones o enfermedades de base existentes
	public ArrayList<Base> getE_b_existentes() {
		return e_b_existentes;
	}


	public void setE_b_existentes(Base b) {
		this.e_b_existentes.add(b);
	}


	public ArrayList<Afeccion> getA_existentes() {
		return a_existentes;
	}


	public void setA_existentes(Afeccion a) {
		this.a_existentes.add(a);
	}
	
	
	public void enfermedades_existentes() {
		/**
		 * Función: definir que personas tienen una afección y/o una enfermedad de base
		 * 
		 * ¿Cómo?: 
		 * Se calcula la cantidad de personas para cada tipo:
		 * -	25 % de la población para enfermedad de base
		 * - 	65 % de la población para afección
		 * luego mediente un for se va agregando una afeccion o emnfermedad de base aleatoria a cada persona.
		 * Para que las personas no las reciban en el mismo orden se desordenan los ArrayList.
		 * Esto hace que la cantidad ee personas sanas (sin afeccion o emnfermedad de base) varie entre un 10 % y un 35 %
		 * */
		
		Collections.shuffle(com.getComunidad());
		
		double e_b = com.getComunidad().size() * 0.25;
		for(int i = 0; i< e_b; i++) {
			int al = (int)(Math.random()*(e_b_existentes.size()));
			com.getComunidad().get(i).setB(e_b_existentes.get(al));
		}
		
		Collections.shuffle(com.getComunidad());
		double a = com.getComunidad().size() * 0.65;
		for(int j = 0; j< a; j++) {
			int al = (int)(Math.random()*(a_existentes.size()));
			com.getComunidad().get(j).setA(a_existentes.get(al));   
		}	
	}
	
	public void probabilidadDeEnfenfar() {
		/**
		 * Función: calcular el indice de gavedad de cada persona antes de infectarse(Enfermedad infecciosa)
		 * 
		 * ¿Cómo? 
		 * Para cada persona se crea una variable double que irá sumando los indices que encuentre.
		 * Cada persona tienen un ArrayList para cada tipo (Base y Afeccion) y
		 * cada enfermedad o afeccion tiene un indice aleatorio, para llegar a él 
		 * se requiere un for que recorra cada lista y lo obtenga
		 * 
		 * Pero la condicion era que las personas sanas tuvieran un 60 % menos de probabilidad 
		 * de enfermar gravemente o morir, entonces no todad van a aumentar su indice.
		 * Para ello se crea un n° aleatorio (alea) entre 1 y 10 y 2 condiciones:
		 *  -	si alea está entre 1 y 4 entonces esa persona si aumenta su indice
		 *  -	si alea está entre 5 y 10, no.
		 * Esto significa que las personas sanas solo tendran un 40 % de probabilidad de subir su indice
		 * 
		 * 
		 * Infecciosa		Afección_max		E_base_max 		   Sano_max
		 * 		1				0.99				0.99			
		 * 		1				0.99
		 * 		1									0.99
		 * 		1													0.99
		 * */
		for(Persona p: com.getComunidad()) {
			
			double total =0;
			
			for(Afeccion a: p.getA()) {
				total = a.getProbabilidad();
				}
			for(Base b: p.getB()) {
				total  += b.getProbabilidad();
				
			}
		
				if(p.getA().isEmpty() && p.getB().isEmpty()) {	
				// se define un nuemro aleatorio entre 1 y 10
					int alea = (int)(Math.random()*10+1);
					// solo el 40 % (n° entre 1 y 4) puede enfermar gravemente o morir
					if(alea>=1 || alea >=4) {
						
						total += Math.round((Math.random()*1)*100.0)/100.0;
					}
						
				}
				
			p.setIndice_gravedad(total);
																																																																																																																								
		}
	}
	

	
	public int sanos() {
		/**
		 * Encargado de contar los sanos (Sin afeccion o enfermedad de base)
		 * para ello de verifica que los ArrayList A y B estén vacíos,
		 * o sea, que no tengan ninguna enfermedad extra
		 * 
		 * */
		int cont3 = 0;
		for(Persona p: com.getComunidad()) {
			if(p.getA().isEmpty() && p.getB().isEmpty()) {
				cont3++;
			}
		}
		return cont3;	
	}
	
	public int muertos() {
		/**
		 * Encargado de contar los muertos
		 * estos tienen dos atributos con un valor particular
		 * Estado y Enfermo son true
		 * los objetos con esta condicion se guardan en _muertos y avanza cont4
		 * Se devuelve el valor final de cont4, */
		int cont4 = 0;
		
		for(Persona p: com.getComunidad()) {
			if(p.isEnfermo() && p.isEstado()) {
				set_muertos(p);
				cont4++;
			}
				
		}
		return cont4;
	}
	
	public void sacar_muertos() {
		/**
		 * Función: quitar los muertos de comunidad al final de la enfermedad
		 * 
		 * ¿Cómo? con un for recorre _muertos (ArrayListo solo con personas muertas) 
		 * y va quitanto a cada una de las personas del ArrayList Comunidad
		 * */
		for(Persona p: _muertos) {
			com.getComunidad().remove(p);
				
			}
		//System.out.println("Total poblacion:" + com.getComunidad().size());
				
	}
		

	public ArrayList<Persona> get_muertos() {
		return _muertos;
	}

	public void set_muertos(Persona _muertos) {
		this._muertos.add(_muertos);
	}
	
	
	public int recuperados() {
		/**
		 * Encargada de contar los recuperados o inmunes ya que no siguen contagiando
		 * Cada estado tiene dos atributos con un valor particular
		 * Estado  is true y Enfermo is false
		 * El contador avanza cuando detecta esta condición y devuelve el valor final
		 * */
		int cont5 = 0;
		for(Persona p: com.getComunidad()) {
			if(p.isEstado() && !p.isEnfermo()) {
				cont5++;
			}
		}
		return cont5;
	}
	
	public int gravedad() {
		/**
		 * Función: Contar las personas en estado grave
		 * Este método va inmediatamente después de yaAvanzaElContador()
		 * Se encarga de buscar a personas que tengas un contador 
		 * mayor a la mitad del promedio de pasos y que su indice de gravedad 
		 * sea mayor a 1, pero que no esté muerto ni recuperado
		 * 
		 * ¿Cómo? se crea un contador antes de recorrer la comunidad
		 *  este contador (cont6) avanza cuando encuentra a la persona descrita anteriormente
		 *  Luego se pregunta si el contador de las personas superó el promedio de pasos
		 *  desntro de esta hay otra 2 preguntas:
		 *   -	si el i_g(indice de gravedad) es menor a 1:
		 *   		en este caso cont6 nunca avanzó
		 *   		la persona se recupera
		 *   -	si i_g > 1, o sea, está grave.
		 *   	el contador de las personas solo se detendrá cuando se recuperen o se mueran}
		 *   	teniendo eso encuenta se puede hacer la siguiente pregunta:
		 *   	-	el contador(de cada persona) superó promedio_pasos*3/2?
		 *   		si es que es así se pregunta otra vez por su i_g, hasya ahora todos son mayores a 1
		 *   		pero son mayores que 2?
		 *   		si es que si, las personas se mueren y se resta 1 a cont6 ya que estas estuvieron graves
		 *   		si es que no, se pregunta por su edad:
		 *   		-	si es <7 (niño pequeño) o >80 (adulto mayor) se muere, también se resta 1 a cont6
		 *   		-	si 7 <= edad <= 80 la peronsa se recupera, también se resta 1 a cont6
		 *   
		 *   como son conujtos de if y else, una condición nunca va a ser la misma que la otra,
		 *   o sea, solo se va a restar 1 vez a cont6 por cada persona que cumpla con las condiciones
		 *   Esto también significa que solo van a quedar las personas que estén graves en este momento
		 *   
		 *   
		 *   Función 2: convertir a las personas a inmunes o muertos
		 * 
		 * ¿Cómo?
		 * Compara el valor del contador de cada persona con el promdeio de pasos para recuperarse
		 * si el contador es mayor, la persona se vuelve inmune (Estado is true) y deja de estar enferma
		 * (Enfermo is false) si responde las otras preguntas, puede morir o vivir
		 * 
		 * Condición:
		 * solo aplica a personas enfermas, ya que el contador no avanza si no se han contagiado 
		 *   
		 *   */
		int cont6 = 0;
		
		for(Persona p: com.getComunidad()) {
			if(p.getCont()>(com.getEnfermedad().getPromedio_pasos()*1/2)) {
				// sabiendo que el contador solo avanza cuando la persona está infectada
				// el contador de graves va a avnzar si encuentra  alguien 
				if(p.getIndice_gravedad() > 1){
					cont6++;
				}
			}
			
			
			
			if(p.getCont()>com.getEnfermedad().getPromedio_pasos()) {
				if(p.getIndice_gravedad()<=1) {
					// si el indice de gravedad es igual o menor que 1 se recupera
					p.setEnfermo(false);
					p.setEstado(true);
				}else {
					// si el indice de gravedad es mayor a 1 y lleva 3/4 más de tiempo infectado
					if(p.getCont()>(com.getEnfermedad().getPromedio_pasos()*3/2)) {
						if(p.getIndice_gravedad()>2) {
							// si el indice es mayor a 2 se muere
							p.setEnfermo(true);
							p.setEstado(true);
							cont6--;
						}else {
							if(p.getEdad()>80 || p.getEdad()<7) {
								// si el indice no es mayor a 2 pero es un adulto mayor o un niño pequeño
								// se muere
								p.setEnfermo(true);
								p.setEstado(true);
								cont6--;
							}else {
								//si el indice no es mayor a 2, pero mayor a 1 y su edad está entre 7 y 80 años se recupera
								p.setEnfermo(false);
								p.setEstado(true);
								cont6--;
							}
						}
					}
				}
			}
		}
		
		return cont6;
	}
	
	public void cantidad_de_vacunas(Vacuna v) {
		/**
		 * Las vacunas están definidas para el 50 % de la población
		 * y está distribuida a razón 9:6:3,o sea, se divide en 18 partes
		 * 9/18 para V1, 6/18 para V2 y 3/18 para V3
		 * la población final que podrá vacunarse con las distintas vacunas está dada por:
		 * -	P--> población		1/2 --> 50%
		 * -	P * 1/2 * 9/18 = P * 1/4
		 * -	P * 1/2 * 6/18 = P * 1/6
		 * -	P * 1/2 * 3/18 = P * 1/12
		 * 
		 * Este método recibe una vacuna, switch pregunta cual es su tipo (1, 2 o 3)
		 * se aplica la divisón correspondiente y se redondea, luego se convierte a int 
		 * y se aplica el método setCantidad de vacuna para actualizar la cantidad
		 * */
		switch(v.getTipo()) {
		case 1: v.setCantidad((int) Math.round((com.getNum_cuidadanos()/4))); break;
		case 2:	v.setCantidad((int)Math.round((com.getNum_cuidadanos()/6))); break;
		case 3: v.setCantidad((int)Math.round((com.getNum_cuidadanos()/12))); break;
		}
		//System.out.println("vacuna tipo: "+ v.getTipo() + " cantidad" + v.getCantidad());
	}

	public HashMap<Integer, Vacuna> getVacunas() {
		return vacunas;
	}

	public void setVacunas(int i, Vacuna v) {
		this.vacunas.put(i, v);
	}
	
	public int conteo_vac() {
		/**
		 * este metodo obtiene la cantidad actual de cada vacuna
		 * las suma y devulve el total*/
		int tot = 0;
		for(Vacuna v: vacunas.values()) {
			tot += v.getCantidad();
			
		}
		return tot;
	}
	
	public void vacunar(int pasos, int mt) {
		//System.out.println("entre a vacunar" );
		/**
		 * mt = la mitad del ciclo run
		 * pasos= los pasos actuales
		 * pasos - mt es necesario para que la edad empiece desde 90,
		 * ya que ya que el cilo se va a activar a la mitad del ciclo run
		 * cuando pasos == mt
		 * 
		 * Solo se vacunará con 1ra dosis a personas que no estén infectadas
		 * o sea, vulnerables e inmunes simpre y cuando hayan vacunas disponibles
		 * 
		 * 
		 * for recorre la comunidad
		 * !p.isEnfermo() -->personas que no están contagiadas ni muertas
		 *  p.getV() == null --> personas que no estén vacunadas
		 *  se pregunta si las personas cumplen con la edad o son resagados
		 *  si es así, se imprimen sus datos y un numero aleatorio (1, 2 o 3) defina que vacuna se pondrá
		 *  esta se obtiene del HashMap de vacunas
		 *  al momento de vacunarse se guarda el día en que se vacunó = pasos + 1
		 *  debido a que pasos empieza en 0 y los días en 1
		 *  adempas en el for hay un contador que se activa cada vez que se vacuna una persona
		 *  y se pregunta por el al principo, esto es para evitar que se ocupen más vacunas de las
		 *  que realmente hay
		 *  
		 *  luego se resta una unidad a la cantidad de vacunas
		 *  
		 *  si la vacuna es V1, se aplican inmediatamente los efectos
		 *  
		 *  Efectos V1:
		 *  	- reducir un 25 % el nivel de la enfemedad infecciosa
		 *  	esto se aplica al multiplicar el indice de gravedad por 0.75
		 *  	y si estapersona llega a infectarse una ves vacunado 
		 *  	solo se sumará 0.75 en vez de 1, así la suma total correspondería
		 *  	a bajar el 25 %
		 *  
		 *  Efectos V2:
		 *  	hace que solo presenten sintomas leves, pero necesita 2 dosis
		 *  	 
		 *  Efectos V3:
		 *  	inmunidad absoluta, pero también necesita 2 dosis
		 *  
		 *  */
		int edad = 90 - (pasos - mt);
		System.out.println("Edad de vacunacion: " + edad);
		int t_p_edad = personas_de_cierta_edad(edad);
		System.out.println("personas de esa edad o más: " + t_p_edad);
		//int t_vacunados = cuenta_vacunados();
		//int mitad_poblacion = (int)(com.getNum_cuidadanos()*0.5);
		int vac_disponibles = conteo_vac();
		int cont = 0;
		System.out.println("v disponibles: " + vac_disponibles);
		if(vac_disponibles >0) {
			for(Persona p: com.getComunidad()) {
				if(cont< vac_disponibles) {
			//if(t_vacunados <= mitad_poblacion) {
			
				if(!p.isEnfermo() && p.getV() == null) {
				
					if( p.getEdad() >= edad ) {
					//System.out.println("id: "+p.get_id() + " edad:" + p.getEdad() + " i_g: " + p.getIndice_gravedad());
						int al = (int)(Math.random()*3+1);
					//System.out.println("Vacuna: " +al);
						
							p.setV(vacunas.get(al));
							
					// día vacunación
							p.setDia_1ra_dosis(pasos+1);
					
					// resta 1 vacuna
							vacunas.get(al).setCantidad(vacunas.get(al).getCantidad()-1);
							cont++;
					
					//System.out.println("Día aplicación primera dosis: "+ p.getDia_1ra_dosis());
							if(al == 1) {
						//System.out.println("Se aplica única dosis vacuna 1");
								p.setIndice_gravedad( (int)(p.getIndice_gravedad()*0.75));
							}
							
							}
					}
				}
			}
		}
	}
	
	
	
	public void segundas_dosis(int pasos) {
		/**
		 * Este método  se encarga de aplicar la segunda dosis a las personas que corresponda
		 * para eso debe:
		 * -	Discriminar entre personans que se han vacunado o no
		 * 		o sea, p.getV() != null
		 * -	Definir una variable para identificar los días que han pasado desde la 1ra dosis
		 * -	preguntar si los días de diferencia (3 V2, 6 V3) calzan con la variable definida anteriormente
		 * -	si es así, preguntra por el tipo de vacuna, 2 o 3
		 * -	aplicar los efectos de la vacuna que corresponda
		 * 
		 * 
		 * Efectos 2da dosis V2:
		 * 		solo presentar sintomas leves, para ello hay que cosiderar que parapresentar
		 * 		síntomas graves el indice de gravedad debe ser estrictamente mayor a 1 
		 * 		Entonces el indice de gravedad se vuelve 0 para que al contagiarse, si es que se contagia,
		 * 		no pase a estado grave
		 * 	
		 * Efectos 2da dosis V3:
		 * 		inmunidad absoluta, para eso se debe cambiar los booleanos Estado y Enfermo 
		 * 		a true y false respectivamente, además volver el indice de gravedad a 0 
		 * 		para evitar errores de calculo.
		 * 		
		 * 		
		 * */
		
		for(Persona p: com.getComunidad()) {
			
			if(p.getV() != null) {
				
			//System.out.println("Día app: "+ p.getDia_1ra_dosis() + " persona: " + p.get_id() + " vacuna: " + p.getV().getTipo());
				int diferencia = (pasos+1) - p.getDia_1ra_dosis(); 
			//System.out.println("días después de la 1ra dosis: " + diferencia);
				if(p.getV().getDosis() == 2) {
				
					if(diferencia == p.getV().getDiferencia()) {
						if(p.getV().getTipo() == 2) {
							//System.out.println(" se aplica segunda dosis v2 a pasiente: " + p.get_id());
							if(p.getIndice_gravedad()> 0) {
								p.setIndice_gravedad(0);
							}
						}else if(p.getV().getTipo() == 3) {
							//System.out.println(" se aplica segunda dosis v3 a pasiente: " + p.get_id());
						
							p.setEnfermo(false);
							p.setEstado(true);
							p.setIndice_gravedad(0);
							}	
						}
					}		
				}
			
		}
	}



	public void impr_datos_poblacion() {
		/**
		 * Calcula las personas con:
		 * 		Enfermedad de base
		 * 		Afeccion
		 * 		Total con Enfermedad de base o Afeccion
		 * 		Sanos (llama a la función sanos())
		 * 		Promedio edad (llama a la función promedioEdad())
		 * 
		 * Y lo imprime*/
		
		int sanos = sanos();
		int e_b = 0;
		int aa = 0;
		int todo = 0;
		
	 
		for(Persona p: com.getComunidad()) {

			if(!p.getA().isEmpty() || !p.getB().isEmpty()) {
				todo++;
			}
			if(!p.getA().isEmpty()) {
				aa++;
			}
			if(!p.getB().isEmpty()) {
				e_b++;
			}
		}
		
		System.out.println("\n***************************************************************************************"+
				"\n	Total poblacion sana: "+ sanos+
				"\n	Total poblacion con afeccion o enfermedad de base: "+ todo+
				"\n	Poblacion con enfermedad de base:" + e_b+
				"\n	Poblacion con una afeccion: "+ aa );
		promedioEdad();
		System.out.println("***************************************************************************************");

	 
	 
	}
	
	public int cuenta_vacunados() {
		/**
		 * Cuenta los vacunados.
		 * Ellos tienen una caracteristic que es que el atributo v no está vacío
		 * */
		int contv = 0;
		for(Persona p: com.getComunidad()) {
			if(p.getV() != null) {
				contv++;
			}
		}
		return contv;
	}
	
	public int personas_de_cierta_edad(int edad) {
		/**
		 * Cuenta a las prsonas que están disponibles para vacunarse
		 * deben tener cierta edad o más (resagados)
		 * no deben estar vacunados
		 * y no deben estar contagiados o muertos (esto se aplica solo para la primera dosis)*/
		int conte = 0;
		for(Persona p: com.getComunidad()) {
			if(p.getEdad() >= edad && p.getV() == null && !p.isEnfermo()){
				
				conte++;
			}
			
		}
		
		return conte;
	}
 }
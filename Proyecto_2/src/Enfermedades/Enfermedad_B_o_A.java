package Enfermedades;


public class Enfermedad_B_o_A {
	private String name;
	private double probabilidad;
	private int tipo;
	
	public Enfermedad_B_o_A(String nombre, double prob) {
		this.name = nombre;
		this.probabilidad = prob;
		//this.tipo =tipo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getProbabilidad() {
		return probabilidad;
	}

	public void setProbabilidad(double probabilidad) {
		this.probabilidad = probabilidad;
	}
	
	public void imprimir() {
		System.out.println("Nombre: " + name + "	 || Probabilidad asignada: " + probabilidad);
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
}

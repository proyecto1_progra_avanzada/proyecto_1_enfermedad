package Vacunas;

public class Vacuna {
	private int tipo;
	private int dosis;
	private int diferencia;
	private int cantidad;
	//private int dia_app;
	/**
	 * Tipo: 
	 * -	1: baja en un 25 % los efectos de la enfermedad infecciosa
	 * -	2: No hay efectos graves, pero si contagia --> indice <= 1
	 * -	3: inmunidad completa
	 * 
	 * Dosis: 
	 * 		1: 1 dosis
	 * 		2: 2 dosis, la 2da 3 días después de la primera
	 * 		3: 2 dosis, la 2da 6 días después de la primera
	 * 
	 * Cantidad: dosis disponibles, se calculan 
	 * 		1: para 1/4 de la población 
	 * 		2: para 1/6 de la población 
	 * 		3: para 1/12 de la población 
	 * 
	 * 
	 * */
	
	public Vacuna(int tipo, int dosis) {
		this.tipo = tipo;
		this.dosis = dosis;
		if(tipo == 2) {
			this.diferencia = 3;
		}else if (tipo == 3) {
			this.diferencia = 6;
		}
		//this.cantidad = cant;
	}
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getDosis() {
		return dosis;
	}
	public void setDosis(int dosis) {
		this.dosis = dosis;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	public int getDiferencia() {
		return diferencia;
	}

	public void setDiferencia(int diferencia) {
		this.diferencia = diferencia;
	}
	
	

}

import Enfermedades.Afeccion;
import Enfermedades.Base;
import Enfermedades.Enfermedad;
import Principal.Comunidad;
import Vacunas.Vacuna;

public class Main {

	public static void main(String[] args) {
		//Enfermedad (infeccion_probable, promedio_pasos);
		Enfermedad e = new Enfermedad(0.2, 10);
		
		//Enfermedades de base
		Base b1, b2, b3, b4, b5;
		Afeccion a1, a2;
		
		double alea = Math.random()*1;  
		b1 = new Base("Asma", Math.round(alea*100.0)/100.0);
		//b1.imprimir();
		alea = Math.random()*1; 
		b2 = new Base("Enfermedad cerebro vascular", Math.round(alea*100.0)/100.0);
		//b2.imprimir();
		alea = Math.random()*1; 
		b3 = new Base("Fibrosis quistica", Math.round(alea*100.0)/100.0);
		//b3.imprimir();
		alea = Math.random()*1; 
		b4 = new Base("Hipertension", Math.round(alea*100.0)/100.0);
		//b4.imprimir();
		alea = Math.random()*1; 
		b5 = new Base("Presion alterial alta", Math.round(alea*100.0)/100.0);
		//b5.imprimir();
		
		alea = Math.random()*1; 
		a1 = new Afeccion("Obesidad", Math.round(alea*100.0)/100.0);
		//a1.imprimir();
		alea = Math.random()*1; 
		a2 = new Afeccion("Desnutricion", Math.round(alea*100.0)/100.0);
		//a2.imprimir();
		
		
		
		
		
		//Comunidad( num_cuidadanos, promedio_conexion_fisica, num_imfectados, probabilidad_conexion_fisica, Enfermedad);
		Comunidad c = new Comunidad( 5000, 6, 2, 0.4, e);
		
		
		
		Simulador s = new Simulador(c);
		s.setE_b_existentes(b1);
		s.setE_b_existentes(b2);
		s.setE_b_existentes(b3);
		s.setE_b_existentes(b4);		
		s.setE_b_existentes(b5);
		
		s.setA_existentes(a1);
		s.setA_existentes(a2);
		
		Vacuna v1, v2, v3;
		v1 = new Vacuna(1, 1);
		s.setVacunas(1, v1);
		v2 = new Vacuna(2, 2);
		s.setVacunas(2, v2);
		v3 = new Vacuna(3, 2);
		s.setVacunas(3, v3);
		
		
		

		
		s.pre_run();
		

		s.run(100);
		
	}

}

package Principal;
import java.util.ArrayList;


import Enfermedades.Afeccion;
import Enfermedades.Base;
import Enfermedades.Enfermedad;
import Vacunas.Vacuna;

public class Persona {
	private Comunidad comunidad;
    private int _id;
	private Enfermedad enfermedad;
    private boolean estado; 
    private boolean enfermo; 
    private int cont;
    
    private int edad;
    private ArrayList<Base> b;
    private ArrayList<Afeccion> a;
    private double indice_gravedad;
    private Vacuna v;
    private int dia_1ra_dosis;
    
    public Persona (int id, int edad){
    	/**
    	 * Constructor de la clase Persona
    	 * solo recibe el id de la persona
    	 * estado se refiere a si la persona es inmune o vulnerable
    	 * inmune (true): ya superó la enfermedad
    	 * vulnerable (false): no ha superado la enfermdedad, esto se da
    	 * por dos casos, no ha estado enfermo o sigue enfermo
    	 * 
    	 * Estado	Enfermo
    	 * false	false	=	vulnerable
    	 * false	true	=	infectado
    	 * true		false	=	inmune
    	 * true		true	=	muerto
    	 * 
    	 * */
    	this._id = id;
    	this.estado = false;
    	this.enfermo = false;
    	this.edad = edad;
    	b = new ArrayList<Base>();
    	a = new ArrayList<Afeccion>();
    	this.indice_gravedad = 0;
 
    	
    }
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public Comunidad getComunidad() {
		return comunidad;
	}

	public void setComunidad(Comunidad comunidad) {
		this.comunidad = comunidad;
	}
	public Enfermedad getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(Enfermedad enfermedad) {
		this.enfermedad = enfermedad;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public boolean isEnfermo() {
		return enfermo;
	}
	public void setEnfermo(boolean enfermo) {
		this.enfermo = enfermo;
	}	
	public int getCont() {
		return cont;
	}
	public void setCont(int cont) {
		this.cont = cont;
	}

	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public ArrayList<Base> getB() {
		return b;
	}
	public void setB(Base b1) {
		this.b.add(b1);
	}
	public ArrayList<Afeccion> getA() {
		return a;
	}
	public void setA(Afeccion a1) {
		this.a.add(a1);
	}
	public double getIndice_gravedad() {
		return indice_gravedad;
	}
	public void setIndice_gravedad(double indice_gravedad) {
		this.indice_gravedad = indice_gravedad;
	}
	public Vacuna getV() {
		return v;
	}
	public void setV(Vacuna v) {
		this.v = v;
	}
	public int getDia_1ra_dosis() {
		return dia_1ra_dosis;
	}
	public void setDia_1ra_dosis(int dia_1ra_dosis) {
		this.dia_1ra_dosis = dia_1ra_dosis;
	}

}